import { Injectable } from '@angular/core';
import { Character } from '../interfaces/character.interface';
import { v4 as uuid} from 'uuid';


@Injectable({
    providedIn: 'root'
})
export class DbzService {


    public characters: Character [] = [{
        id: uuid(),
        name: 'Kilian',
        power: 100
    },
    {
        id: uuid(),
        name:'Gokuu',
        power: 9500
    },
    {
        id: uuid(),
        name: 'Vegeta',
        power: 7500
    }];


  addCharacter( Character: Character) : void {
    const newCharacter : Character = { id: uuid(), ...Character}
    this.characters.push(newCharacter);
    }


    deleteCharacterById( id: string ) {
        this.characters = this.characters.filter ( character => character.id !== id)
    }

    // onDeleteCharacter (index:number) {
    //     this.characters.splice(index,1);
    // }


    constructor() { }
    
}