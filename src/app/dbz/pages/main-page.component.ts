import { Component, OnInit } from '@angular/core';
import { Character } from '../interfaces/character.interface';
import { DbzModule } from '../dbz.module';
import { DbzService } from '../services/dbz.service';

@Component({
    selector: 'app-dbz-main-page',
    templateUrl: './main-page.component.html'
})

export class MainPageComponent {

    constructor ( private DbzService: DbzService) { }

    get characters() : Character[] {
        return [...this.DbzService.characters];
    }

    onDeleteCharacters( id: string ): void {
        this.DbzService.deleteCharacterById(id);
    }

    onNewCharacters (character : Character ) {
        this.DbzService.addCharacter(character);
    }



}