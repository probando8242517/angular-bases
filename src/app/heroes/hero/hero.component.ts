import { Component } from '@angular/core';

@Component({
  selector: 'app-heroes-hero',
  templateUrl: './hero.component.html',
  styleUrl: './hero.component.css'
})

export class HeroComponent {
  public name: string = ' Iroman';
  public age: number=32;

  get capitalizedName():string {
    return "i don't worry".toUpperCase();
  }

  getHeroDescription():string {
    return `${this.name} - ${this.age}`;
  }

  changeHero():void {
  this.name = 'Private';
  }

  changeAge() {
    this.age =8;
  }

  resetForm(): void{
    this.age = 45;

    document.querySelectorAll('h1')!.forEach(Elemento => {
    Elemento.innerHTML='<h1>Desde Angular</h1>'
    });
  }


} 
