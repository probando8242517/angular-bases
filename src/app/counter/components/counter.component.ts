import { Component } from "@angular/core";

@Component({ 
    selector:'app-counter',
    template:`
    <hr>
    <h1>Hola OtroComponent👁‍🗨</h1>
    <h5> que v1va la dr0g{{counter}}</h5>
    <button (click)="increaseBy(+1)">+</button>
<button (click)="reset()">R</button>
<button (click)="increaseBy(-1)">-</button>

    
    
    `
})
export class CounterComponent{
    public counter:number =7;

    increaseBy(value : number):void{
      this.counter+=value;
     
    };
  
    reset() {
      this.counter = 4;
    }
}